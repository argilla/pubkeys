# pubkeys

Banalmente, le chiavi pubbliche di chi si occupa di manutenzione dei sistemi informatici in Officina Informatica.

Vuoi entrare a farne parte? Chiedi ad uno degli amministratori di sistema di aggiungerti.

Sei in questa lista e non vuoi più farne parte? Beh, cancellati da solo!
